# Bonus

In this bonus assignment, we will automate the deployment using a Gitlab CI/CD pipeline and we will be able to set up multiple environments. 

## Additional prerequisites for _bonus_ assignments

- [Gitlab account](https://www.gitlab.com)
- Service Principal with Contributor permissions on the used Azure Subscription
- A Resource Group, Storage Account and Container for the Terraform State (suggestion: rg-iacworkshop-tfstate/_uniquestorageaccountname_/tfstate)

## Shared infra (ACR)

1. Fork the [project](https://gitlab.com/pjrellum/iac-workshop-shared/-/forks/new) for the API and switch to branch Main.
1. In Gitlab Project -> Settings -> CI/CD add fill in the variables. `Mask` the secret.
![pipeline variables](pipeline-variables.png)
1. Ensure the correct storage account name for the Terraform state in `backend.cfg`
1. Review the new `.gitlab-ci.yml` file and notice the pipeline steps.
1. Update the `RANDOM_STRING` variable in `.gitlab-ci.yml` to something new and unique again, commit the change to the Main branch.
1. Watch the pipeline run in CI/CD, manually trigger the `apply` step but don't trigger the `destroy` step yet.

## API (Container image)

1. Fork the [project](https://gitlab.com/pjrellum/iac-workshop-api/-/forks/new) for the API and switch to branch Main.
1. In Gitlab Project -> Settings -> CI/CD add fill in the variables. `Mask` the secret.
1. Add **one extra** pipeline variable named AZURE_CONTAINER_REGISTRY and set it to the ACR name from the previous pipeline (`youracrname.acr.io`)
1. Review the new `.gitlab-ci.yml` file and notice the pipeline steps.
1. Review the `version.info` file to notice the tag that will be applied to the container image.
1. If you don't make any changes, manually run the pipeline for the Main branch from CI/CD.
1. Watch the pipeline run in CI/CD, manually trigger the `apply` step but don't trigger the `destroy` step yet.

## Infrastructure

1. Fork the [project](https://gitlab.com/pjrellum/iac-workshop-infrastructure/-/forks/new) for the API and switch to branch Main.
1. In Gitlab Project -> Settings -> CI/CD add fill in the variables. `Mask` the secret.
1. Ensure the correct storage account name for the Terraform state in `backend.cfg`
1. Review the new `.gitlab-ci.yml` file and notice the pipeline steps.
1. Update the `RANDOM_STRING` variable in `.gitlab-ci.yml` to the same value as for the Shared Infra, commit the change to the Main branch.
1. Watch the pipeline run in CI/CD, manually trigger the `apply` for the DEV environment.
1. When you have a Trial or Pay-As-You-Go subscription, the quota for Azure Container Apps is limited and you cannot deploy the next (TST) environment. 

## Front-end Application

1. Fork the [project](https://gitlab.com/pjrellum/iac-workshop-app/-/forks/new) for the API and switch to branch Main.
1. In Gitlab Project -> Settings -> CI/CD add fill in the variables. `Mask` the secret.
1. Review the new `.gitlab-ci.yml` file and notice the pipeline steps.
1. Update RANDOM_STRING in `.gitlab-ci.yml` and commit your change.
1. Watch the pipeline run in CI/CD, manually trigger the `apply` step but don't trigger the `destroy` step yet.
