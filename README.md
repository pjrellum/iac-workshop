# Infrastructure as Code workshop using Microsoft Azure, Terraform and Gitlab

This workshop demonstrates the deployment of a complete three-tier application (front-end, API, database) using Infrastructure as Code and (bonus) CI/CD pipelines.

![architecture](architecture.png)

- Web App (client)
- Container Apps (API)
- Table Storage (database)
- Container Registry with Container Image

For simplicity and demo purposes, we use an Azure Table for storage.

## Prerequisites

- [Azure CLI](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli)
- [Visual Studio Code](https://code.visualstudio.com/Download)
- [Terraform](https://www.terraform.io/downloads) - Make sure to add the executable to your PATH

## Steps

1. Deploy the Shared Infrastructure - code and instructions [here](https://gitlab.com/pjrellum/iac-workshop-shared/-/archive/01-local/iac-workshop-shared-01-local.zip)
1. Build the API and create a Docker Image - code and instructions [here](https://gitlab.com/pjrellum/iac-workshop-api/-/archive/01-local/iac-workshop-api-01-local.zip)
1. Deploy the application infrastructure - code and instructions [here](https://gitlab.com/pjrellum/iac-workshop-infrastructure/-/archive/01-local/iac-workshop-infrastructure-01-local.zip)
1. Deploy the Front-end application - code and instructions [here](https://gitlab.com/pjrellum/iac-workshop-app/-/archive/01-local/iac-workshop-app-01-local.zip)
1. Destroy all resources from the CLI using `terraform destroy` and review in the Azure Portal
